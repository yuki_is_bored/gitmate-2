from django.apps import AppConfig


class GitmateTestpluginConfig(AppConfig):
    name = 'gitmate_testplugin'

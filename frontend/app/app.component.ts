import { Component } from '@angular/core';

@Component({
  selector: 'my-app',
  templateUrl: `./static/app/app.component.html`,
  styleUrls: [`./static/app/app.component.css`]
})
export class AppComponent  { name = 'Angular'; }

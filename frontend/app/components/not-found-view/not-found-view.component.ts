import { Component } from '@angular/core';

@Component({
  selector: 'view-not-found',
  template: `<h1>Sorry Dave, I'm afraid I cannot let you do that...</h1>`,
})
export class NotFoundComponent { }

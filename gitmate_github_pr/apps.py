from django.apps import AppConfig


class GitmateGithubPrConfig(AppConfig):
    name = 'gitmate_github_pr'
